## List of routes
To visualize the list of routes currently in our app, run the following command:
```sh
$ php bin/console debug:router
```
Or, if you have the symfony CLI installed:
```sh
$ symfony console debug:router
```
