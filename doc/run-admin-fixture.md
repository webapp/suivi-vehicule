## Run admin user fixture
To access the routes under the path `/admin`, you'll need an user with role `ROLE_ADMIN`. There's no form in the app that can allow you to create a user with such role. You'll either have to:
- insert the user manually in the database.
- execute a fixture, which we'll discuss here.

Fixtures are defined in the `src/DataFixtures/AppFixtures.php`. To create a user with admin privileges, it contains the following code:
```php
$user = new User();
$user->setEmail("admin@webapp.com");
$user->setLastname("");
$user->setFirstname("");
$user->setLocation("");
$user->setPhone("");
$user->setPassword("$2y$13$2ee3AC4PHc2Hktie9xtFzublSf5HPRGo9.0V91V7QbcyyIISsd1Hu");
$user->setRoles(["ROLE_ADMIN"]);

$manager->persist($user);

$manager->flush();
```
Here, we create a user with email `admin@webapp.com` and a password. You need to give an already hashed password. To generate your own hash you can use this command from the symfony CLI:
```sh
$ php bin/console security:hash
```
As this file is committed, don't update the password, unless you put it in your own `.env` file. For now, the password is `admin`. (change it when in production !)

Finally, run the fixtures, you'll see a warning stating that your database will be purged, type `yes` and then enter:
```sh
$ php bin/console doctrine:fixtures:load
```

You can now login using the defined email and password and have access to the routes under `/admin`.