<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230316215128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE car_brand (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(32) NOT NULL, UNIQUE INDEX UNIQ_C3F97C8F5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE car_fuel (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, UNIQUE INDEX UNIQ_FAD77B455E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE car_model (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, UNIQUE INDEX UNIQ_83EF70E5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fuel_log_book (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fuel_operation (id INT AUTO_INCREMENT NOT NULL, fuel_id INT NOT NULL, get_fuel_log_book_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, date DATETIME NOT NULL, total_mileage DOUBLE PRECISION NOT NULL, station VARCHAR(255) NOT NULL, quantity_added DOUBLE PRECISION NOT NULL, INDEX IDX_5FD7022E97C79677 (fuel_id), INDEX IDX_5FD7022E12FDABB5 (get_fuel_log_book_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, date DATETIME NOT NULL, total_mileage DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE spending_operation (id INT AUTO_INCREMENT NOT NULL, get_spending_record_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, date DATETIME NOT NULL, total_mileage DOUBLE PRECISION NOT NULL, garage VARCHAR(255) NOT NULL, cost_work_force DOUBLE PRECISION NOT NULL, cost_materials DOUBLE PRECISION NOT NULL, INDEX IDX_DF18C63C11AEFA35 (get_spending_record_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE spending_record (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(128) NOT NULL, lastname VARCHAR(64) NOT NULL, phone VARCHAR(32) NOT NULL, location VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle (id INT AUTO_INCREMENT NOT NULL, fuel_log_book_id INT DEFAULT NULL, spending_record_id INT DEFAULT NULL, fuel_id INT NOT NULL, model_id INT NOT NULL, user_id INT DEFAULT NULL, number VARCHAR(16) NOT NULL, name VARCHAR(128) NOT NULL, UNIQUE INDEX UNIQ_1B80E4862480B677 (fuel_log_book_id), UNIQUE INDEX UNIQ_1B80E4868B64155D (spending_record_id), INDEX IDX_1B80E48697C79677 (fuel_id), INDEX IDX_1B80E4867975B7E7 (model_id), INDEX IDX_1B80E486A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fuel_operation ADD CONSTRAINT FK_5FD7022E97C79677 FOREIGN KEY (fuel_id) REFERENCES car_fuel (id)');
        $this->addSql('ALTER TABLE fuel_operation ADD CONSTRAINT FK_5FD7022E12FDABB5 FOREIGN KEY (get_fuel_log_book_id) REFERENCES fuel_log_book (id)');
        $this->addSql('ALTER TABLE spending_operation ADD CONSTRAINT FK_DF18C63C11AEFA35 FOREIGN KEY (get_spending_record_id) REFERENCES spending_record (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E4862480B677 FOREIGN KEY (fuel_log_book_id) REFERENCES fuel_log_book (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E4868B64155D FOREIGN KEY (spending_record_id) REFERENCES spending_record (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E48697C79677 FOREIGN KEY (fuel_id) REFERENCES car_fuel (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E4867975B7E7 FOREIGN KEY (model_id) REFERENCES car_model (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE `admin`');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `admin` (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, roles JSON NOT NULL, password VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, UNIQUE INDEX UNIQ_880E0D76F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE fuel_operation DROP FOREIGN KEY FK_5FD7022E97C79677');
        $this->addSql('ALTER TABLE fuel_operation DROP FOREIGN KEY FK_5FD7022E12FDABB5');
        $this->addSql('ALTER TABLE spending_operation DROP FOREIGN KEY FK_DF18C63C11AEFA35');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E4862480B677');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E4868B64155D');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E48697C79677');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E4867975B7E7');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486A76ED395');
        $this->addSql('DROP TABLE car_brand');
        $this->addSql('DROP TABLE car_fuel');
        $this->addSql('DROP TABLE car_model');
        $this->addSql('DROP TABLE fuel_log_book');
        $this->addSql('DROP TABLE fuel_operation');
        $this->addSql('DROP TABLE operation');
        $this->addSql('DROP TABLE spending_operation');
        $this->addSql('DROP TABLE spending_record');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE vehicle');
    }
}
