<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReleveMensuelController extends AbstractController
{
    #[Route('/user/releve-mensuel', name: 'app_user_releve_mensuel')]
    public function index(): Response
    {
        return $this->render('releve_mensuel/index.html.twig', [
            'controller_name' => 'ReleveMensuelController',
        ]);
    }
}
