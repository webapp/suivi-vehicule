<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    #[Route('/admin/user', name: 'app_admin_user')]
    public function indexUser(): Response
    {
        return $this->render('admin_user/index.html.twig', [
            'controller_name' => 'AdminController'
        ]);
    }

    #[Route('/admin/fuel', name: 'app_admin_fuel')]
    public function indexFuel(): Response
    {
        return $this->render('admin_fuel_page/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    #[Route('/admin/vehicle', name: 'app_admin_vehicle')]
    public function indexVehicle(): Response
    {
        return $this->render('admin_vehicle/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
}
