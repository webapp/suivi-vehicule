<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FuelController extends AbstractController
{
    #[Route('/user/fuel', name: 'app_user_fuel')]
    public function index(): Response
    {
        return $this->render('fuel_page/index.html.twig', [
            'controller_name' => 'FuelController',
        ]);
    }
}
