<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarnetDepenseController extends AbstractController
{
    #[Route('/user/carnet-depense', name: 'app_user_carnet_depense')]
    public function index(): Response
    {
        return $this->render('carnet_depense/index.html.twig', [
            'controller_name' => 'CarnetDepenseController',
        ]);
    }
}
