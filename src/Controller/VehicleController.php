<?php

namespace App\Controller;

use App\Entity\Vehicle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VehicleController extends AbstractController
{
    #[Route('/user/vehicule', name: 'app_user_vehicle')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED');

        return $this->render('vehicle/index.html.twig', [
            'controller_name' => 'VehicleController',
        ]);
    }

    #[Route('/user/vehicule/{id}', name: 'app_user_vehicule_show')]
    public function show(EntityManagerInterface $entityManager, int $id): Response
    {

        $vehicule = $entityManager->getRepository(Vehicle::class)->find($id);

        if (!$vehicule) {
            throw $this->createNotFoundException(
                "Aucun véhicule pour l'identifiant " . $id
            );
        }

        return $this->render('vehicle/show.html.twig', [
            'vehicle_mat' => $vehicule->getNumber(),
            'vehicle_name' => $vehicule->getName()
        ]);
    }
}
