<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();
        $user->setEmail("admin@webapp.com");
        $user->setLastname("");
        $user->setFirstname("");
        $user->setLocation("");
        $user->setPhone("");
        $user->setPassword("$2y$13$2ee3AC4PHc2Hktie9xtFzublSf5HPRGo9.0V91V7QbcyyIISsd1Hu");
        $user->setRoles(["ROLE_ADMIN"]);

        $manager->persist($user);

        $manager->flush();
    }
}
