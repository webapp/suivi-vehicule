<?php

namespace App\Entity;

use App\Repository\SpendingRecordRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SpendingRecordRepository::class)]
class SpendingRecord
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'getSpendingRecord', targetEntity: SpendingOperation::class)]
    private Collection $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, SpendingOperation>
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(SpendingOperation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations->add($operation);
            $operation->setGetSpendingRecord($this);
        }

        return $this;
    }

    public function removeOperation(SpendingOperation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getGetSpendingRecord() === $this) {
                $operation->setGetSpendingRecord(null);
            }
        }

        return $this;
    }
}
