<?php

namespace App\Entity;

use App\Repository\SpendingOperationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SpendingOperationRepository::class)]
class SpendingOperation extends Operation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $garage = null;

    #[ORM\Column]
    private ?float $costWorkForce = null;

    #[ORM\Column]
    private ?float $costMaterials = null;

    #[ORM\ManyToOne(inversedBy: 'operations')]
    private ?SpendingRecord $getSpendingRecord = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGarage(): ?string
    {
        return $this->garage;
    }

    public function setGarage(string $garage): self
    {
        $this->garage = $garage;

        return $this;
    }

    public function getCostWorkForce(): ?float
    {
        return $this->costWorkForce;
    }

    public function setCostWorkForce(float $costWorkForce): self
    {
        $this->costWorkForce = $costWorkForce;

        return $this;
    }

    public function getCostMaterials(): ?float
    {
        return $this->costMaterials;
    }

    public function setCostMaterials(float $costMaterials): self
    {
        $this->costMaterials = $costMaterials;

        return $this;
    }

    public function getTotalCost(): floatval
    {
        return $this->costWorkForce + $this->costMaterials;
    }

    public function getGetSpendingRecord(): ?SpendingRecord
    {
        return $this->getSpendingRecord;
    }

    public function setGetSpendingRecord(?SpendingRecord $getSpendingRecord): self
    {
        $this->getSpendingRecord = $getSpendingRecord;

        return $this;
    }
}
