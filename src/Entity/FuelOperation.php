<?php

namespace App\Entity;

use App\Repository\FuelOperationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FuelOperationRepository::class)]
class FuelOperation extends Operation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $station = null;

    #[ORM\Column]
    private ?float $quantityAdded = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?CarFuel $fuel = null;

    #[ORM\ManyToOne(inversedBy: 'operations')]
    private ?FuelLogBook $getFuelLogBook = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStation(): ?string
    {
        return $this->station;
    }

    public function setStation(string $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getQuantityAdded(): ?float
    {
        return $this->quantityAdded;
    }

    public function setQuantityAdded(float $quantityAdded): self
    {
        $this->quantityAdded = $quantityAdded;

        return $this;
    }

    public function getFuel(): ?CarFuel
    {
        return $this->fuel;
    }

    public function setFuel(?CarFuel $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getGetFuelLogBook(): ?FuelLogBook
    {
        return $this->getFuelLogBook;
    }

    public function setGetFuelLogBook(?FuelLogBook $getFuelLogBook): self
    {
        $this->getFuelLogBook = $getFuelLogBook;

        return $this;
    }
}
