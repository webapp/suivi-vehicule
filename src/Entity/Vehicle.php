<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VehicleRepository::class)]
class Vehicle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 16)]
    private ?string $number = null;

    #[ORM\Column(length: 128)]
    private ?string $name = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?FuelLogBook $fuelLogBook = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?SpendingRecord $spendingRecord = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?CarFuel $fuel = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?CarModel $model = null;

    #[ORM\ManyToOne(inversedBy: 'fleet')]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function equals(Vehicle $vehicule): bool
    {
        if (($this->number == $vehicule->number) && ($this->name == $vehicule->name)) {
            return true;
        }
        return false;
    }

    public function getFuelLogBook(): ?FuelLogBook
    {
        return $this->fuelLogBook;
    }

    public function setFuelLogBook(FuelLogBook $fuelLogBook): self
    {
        $this->fuelLogBook = $fuelLogBook;

        return $this;
    }

    public function getSpendingRecord(): ?SpendingRecord
    {
        return $this->spendingRecord;
    }

    public function setSpendingRecord(SpendingRecord $spendingRecord): self
    {
        $this->spendingRecord = $spendingRecord;

        return $this;
    }

    //TODO generateMonthlyRecord

    public function getFuel(): ?CarFuel
    {
        return $this->fuel;
    }

    public function setFuel(?CarFuel $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getModel(): ?CarModel
    {
        return $this->model;
    }

    public function setModel(?CarModel $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
