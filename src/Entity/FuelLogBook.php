<?php

namespace App\Entity;

use App\Repository\FuelLogBookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FuelLogBookRepository::class)]
class FuelLogBook
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'getFuelLogBook', targetEntity: FuelOperation::class)]
    private Collection $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, FuelOperation>
     */
    public function getFuelOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(FuelOperation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations->add($operation);
            $operation->setGetFuelLogBook($this);
        }

        return $this;
    }

    public function removeOperation(FuelOperation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getGetFuelLogBook() === $this) {
                $operation->setGetFuelLogBook(null);
            }
        }

        return $this;
    }
}
