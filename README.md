# Projet Suivi de Véhicule
The present repository contains the source code for our project for class **PA-DI-WEBAPP-B** (_Ingénierie des Applications Web_) at IMT Atlantique.

## Requirements
The following requirements has to present on your system:
- php v8.1
- composer v2.5.4
- symfony v6.2
- symfony CLI (optional) v5.5.1
- node v18.12.1
- npm 8.19.2

## Get started
Clone the repository, `cd` into the directory and then run:
```sh
$ composer install
```

Check the `.env.example` file to see which environment variables are required copy the content of the file to your env file, either `.env` or `.env.local` and update it accordingly to your development environment:
```sh
$ cp .env.example .env
```

If you are running the project for the first time, you may want to tell doctrine to create the database first (don't forget to configure your `server_version` parameter in `config/packages/doctrine.yaml`):
```sh
$ php bin/console doctrine:database:create
```

Run the migrations:
```sh
$ php bin/console doctrine:migrations:migrate
```

Start the server by running the following command:
```sh
$ symfony server:start
```

Next, set up Bootstrap. For that, you'll need npm installed on your machine. In your project's folder:
```sh
$ cd public/assets
$ npm install
```

## Contributing
Follow the [conventional commit guideline](https://www.conventionalcommits.org/en/v1.0.0/).


## License
The present source code is distributed under the GNU License.

